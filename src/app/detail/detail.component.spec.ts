import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DetailComponent } from './detail.component';
import { DataService } from '../shared/services/data.service';
import { asyncData } from '../testing/utils';

describe('DetailComponent', () => {
  let component: DetailComponent;
  let fixture: ComponentFixture<DetailComponent>;
  let serviceSpy: jasmine.SpyObj<DataService>;

  beforeEach(async(() => {
    const dataService = jasmine.createSpyObj('DataService', ['getDetail']);
    TestBed.configureTestingModule({
      providers: [ { provide: DataService, useValue: dataService } ],
      declarations: [ DetailComponent ],
      imports: [ RouterTestingModule ],

    })
    .compileComponents();
    serviceSpy = TestBed.get(DataService)
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    serviceSpy.getDetail.and.returnValue(asyncData(null));
    expect(component).toBeTruthy();
  });
});
