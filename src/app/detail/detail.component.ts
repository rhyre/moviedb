import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/services/data.service';
import { DetailItem } from '../shared/interfaces/data.interface';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
  ) { }

  item: DetailItem = null;

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap(params => this.dataService.getDetail(params.get('id'))))
    .subscribe(res => this.item = res, err => console.log(err));
  }

}
