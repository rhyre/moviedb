import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService) { }

  ngOnInit() {

  }

  onClickAuthenticate(token: string) {
    this.auth.validateToken(token).subscribe(
      res => this.auth.authenticate(token),
      err => alert('wrong token'));
  }

}
