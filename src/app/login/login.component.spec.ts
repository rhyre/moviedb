import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthService } from '../shared/services/auth.service';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let serviceSpy: jasmine.SpyObj<AuthService>;

  beforeEach(async(() => {
    const authService = jasmine.createSpyObj('AuthService', ['authenticate', 'validateToken']);
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers: [ { provide: AuthService, useValue: authService } ],
      imports: [FormsModule, ReactiveFormsModule],
    })
    .compileComponents();
    serviceSpy = TestBed.get(AuthService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
