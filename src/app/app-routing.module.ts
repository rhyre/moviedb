import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { DetailComponent } from './detail/detail.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './shared/services/guard.service'


const routes: Routes = [
  { path: 'search', component: SearchComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'detail/:id',        component: DetailComponent, canActivate: [AuthGuardService] },
  { path: '',   redirectTo: '/search', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
