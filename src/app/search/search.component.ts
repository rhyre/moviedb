import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/services/data.service';
import { SearchData } from '../shared/interfaces/data.interface';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {

  constructor(private dataService: DataService) { }

  data: SearchData = null;
  currentPage = 1;

  ngOnInit() {

  }

  getFlooredPage(count) {
    if (count) {
      return Math.floor((+count + 9) / 10);
    }
    return 1;
  }

  onSubmit(filters) {
    this.currentPage = +filters.page || 1;
    this.dataService.getList(filters).subscribe(
      res => this.data = res,
      err => console.log(err),
    )
  }

}
