import { Component, OnInit, Input } from '@angular/core';
import { SearchItem } from '../../shared/interfaces/data.interface';

@Component({
  selector: 'app-search-item',
  templateUrl: './search-item.component.html',
  styleUrls: ['./search-item.component.scss']
})
export class SearchItemComponent implements OnInit {

  constructor() { }

  @Input() item: SearchItem = null;

  ngOnInit() {
  }

}
