import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthGuardService } from './guard.service';
import { AuthService } from './auth.service';
import { Component } from '@angular/core';


@Component({template: ''})
export class SimpleCmp {
  constructor() { }
}


describe('AuthGuardService', () => {

  let authServiceSpy: jasmine.SpyObj<AuthService>;

  beforeEach(() => {
    const authService = jasmine.createSpyObj('AuthService', ['isAuthenticated']);
    TestBed.configureTestingModule({
      declarations: [SimpleCmp],
      imports: [
        RouterTestingModule.withRoutes(
          [{path: 'login', component: SimpleCmp}]
        )
      ],
      providers: [ { provide: AuthService, useValue: authService } ],
    })
    authServiceSpy = TestBed.get(AuthService);
  });

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });

  it('should authenticate', () => {
    authServiceSpy.isAuthenticated.and.returnValue(true);
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service.canActivate()).toBeTruthy();
  });

  it('should not authenticate', () => {
    authServiceSpy.isAuthenticated.and.returnValue(false);
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service.canActivate()).toBeFalsy();
  });
});
