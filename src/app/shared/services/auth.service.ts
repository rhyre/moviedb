import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  token: string = null;
  API_LINK = 'http://www.omdbapi.com/';

  constructor(
    private router: Router,
    private http: HttpClient,
  ) { }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  validateToken(token: string): Observable<any> {
    return this.http.get<any>(this.API_LINK, {params: {apikey: token}});
  }

  authenticate(token: string) {
    this.token = token;
    this.router.navigate(['/']);
  }
}
