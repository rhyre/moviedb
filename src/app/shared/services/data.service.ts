import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SearchData, DetailItem } from '../interfaces/data.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  API_LINK = 'http://www.omdbapi.com/';

  constructor(private http: HttpClient) { }

  getList(query): Observable<SearchData> {
    return this.http.get<SearchData>(this.API_LINK, {params: query});
  }

  getDetail(id: string): Observable<DetailItem> {
    return this.http.get<DetailItem>(this.API_LINK, {params: {i: id || ''}});
  }
}
