import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Component } from '@angular/core';


@Component({template: ''})
export class SimpleCmp {
  constructor() { }
}


describe('AuthService', () => {

  let serviceSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    const httpService = jasmine.createSpyObj('HttpClient', ['get']);
    TestBed.configureTestingModule({
      declarations: [SimpleCmp],
      imports: [
        RouterTestingModule.withRoutes(
          [{path: '', component: SimpleCmp}]
        )],
      providers: [ { provide: HttpClient, useValue: httpService } ],
    });
    serviceSpy = TestBed.get(HttpClient);
  });

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });

  it('should authenticate', () => {
    const service: AuthService = TestBed.get(AuthService);
    service.authenticate('token');
    expect(service.token).toBe('token');
  })

  it('should return true for isAuthenticated', () => {
    const service: AuthService = TestBed.get(AuthService);
    service.token = 'token';
    expect(service.isAuthenticated()).toBeTruthy();
  })

  it('should return false for isAuthenticated', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service.isAuthenticated()).toBeFalsy();
  })


});
