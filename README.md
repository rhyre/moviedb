# Moviedb

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `docker-compose build ng` to build the project.

## Run

Run `docker-compose up` to run in production state. Visit localhost:8080 to see Moviedb
